const site = {
    onload: document.addEventListener('DOMContentLoaded', function () {
        site.init();
    }),
    init: function () {
        this.menu();
        this.sticky();
    },
    menu: function () {
        $(document).on('click', '#navbarBurger', function () {
            $(this).toggleClass('navbar-burger__opened');
            $('.navbar-menu').slideToggle();
        })
    },
    sticky: function () {
        var oldScroll = 0;
        $(window).on('scroll', function () {
            var st = $(this).scrollTop();
            if ($(this).scrollTop() > 0) {
                $('.header').addClass('header__sticky');
                if(st > oldScroll){
                    $('.header').addClass('header__sticky__hide');
                    $('.header').removeClass('header__sticky__show');
                } else {
                    $('.header').addClass('header__sticky__show');
                    $('.header').removeClass('header__sticky__hide');
                }
            } else {
                $('.header').removeClass('header__sticky');
            }
            oldScroll = st;
        });
    }
};